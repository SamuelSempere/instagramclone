import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import FirebaseContext from './context/firebase'


ReactDOM.render(
<FirebaseContext.Provider value={{firebase,FieldValue}}>    
<App />
</FirebaseContext.Provider>,
document.getElementById('root'));

// client side rendered app : react (cra)
    // database -> Firebase
    // react-loading-skeleton
    // trailwind css

//architecture
    //src
        // -> components,
        // -> constants, 
        // -> context, 
        // -> helpers, 
        // -> lib (firebase is going to live in here)
        // -> services (firebase functions here)
        // -> styles (tailwoind's folder (app7/tailwind))